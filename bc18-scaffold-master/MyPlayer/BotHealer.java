import bc.*;

class BotHealer extends BotBase{
    BotHealer(GameController theGC){
        super(theGC);
    }

    void loop(){
        //go to nearby friends
        if (thisUnit.location().isOnMap()) {
            if (thisUnit.location().isOnPlanet(Planet.Earth)){
                boardNearbyRockets();
            }
            VecUnit nearbyTeammates = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.attackRange(), GameConstants.myTeam);
            long currentLowestTeammateHealth = Long.MAX_VALUE;
            Unit healTarget = null;
            if (nearbyTeammates.size() > 0) {
                for (int i = 0; i < nearbyTeammates.size(); i++) {
                    if (nearbyTeammates.get(i).health() < currentLowestTeammateHealth){
                        currentLowestTeammateHealth = nearbyTeammates.get(i).health();
                        healTarget = nearbyTeammates.get(i);
                    }
                }
                if (gc.canHeal(thisUnit.id(),healTarget.id()) && gc.isHealReady(thisUnit.id())) {
                    gc.heal(thisUnit.id(),healTarget.id());
                    //System.out.println("HEALED");
                }
            }





            Unit closestFriend = null;
            long currentClosestDist = Long.MAX_VALUE;
            for (UnitBase friend : Player.myUnitsHash.values()) {
                //for now only heal rangers
                Unit friendlyUnit = friend.thisUnit;
                if (friendlyUnit.location().isOnMap() && friendlyUnit.unitType().equals(UnitType.Ranger)) {
                    long distToThisFriend = thisUnit.location().mapLocation().distanceSquaredTo(friend.thisUnit.location().mapLocation());
                    if (distToThisFriend < currentClosestDist) {
                        currentClosestDist = distToThisFriend;
                        closestFriend = friend.thisUnit;
                    }
                }
            }
            if (closestFriend != null) {
                setTargetLoc(closestFriend.location().mapLocation(),false);
                takeStepToTarget();
            }

        }


    }


}
