import bc.MapLocation;
import bc.Planet;
import bc.PlanetMap;
import bc.VecMapLocation;

import java.util.HashSet;

/**
 * Exactly the same as MapLocation but with A* stats like f/g/h score
 * Created by raytr on 1/10/2018.
 */
class AStarMapLoc extends MapLocation {
    private MapLocation mapLocation;
    public AStarMapLoc(Planet planet, int X, int Y){
        super(planet,X,Y);
        mapLocation = new MapLocation(planet,X,Y);
    }
    public MapLocation mapLocation(){
        return mapLocation;
    }


    public HashSet<AStarMapLoc> getNeighbors(){
        HashSet<AStarMapLoc> finalReturnHash = new HashSet<AStarMapLoc>();
        VecMapLocation mapLocs = GameConstants.gc.allLocationsWithin(mapLocation,2);
        for (int i=0;i<mapLocs.size();i++){
            MapLocation currentMapLoc = mapLocs.get(i);

            PlanetMap pm;
            if (currentMapLoc.getPlanet().equals(Planet.Earth)) pm = GameConstants.earthPM;
            else{
                pm = GameConstants.marsPM;
            }

            //PlanetMap pm = UnitBase.gc.startingMap(currentMapLoc.getPlanet());
            if(pm.isPassableTerrainAt(currentMapLoc) == 1 /*&& !Player.gc.hasUnitAtLocation(currentMapLoc) don't uncomment this*/){
                AStarMapLoc newLoc = new AStarMapLoc(currentMapLoc.getPlanet(),currentMapLoc.getX(),currentMapLoc.getY());
                newLoc.parent = this;
                finalReturnHash.add(newLoc);
            }
        }
        return finalReturnHash;
    }

    public long f;
    public long g;
    public long h;
    public AStarMapLoc parent;
}
