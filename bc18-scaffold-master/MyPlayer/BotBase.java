import bc.*;

import java.util.*;

class BotBase extends UnitBase{
    BotBase(GameController theGC){
        super(theGC);
    }

    private static final AStarMapLocComparator A_STAR_MAP_LOC_COMPARATOR = new AStarMapLocComparator();
    private ArrayList<MapLocation> targetPath;
    private MapLocation targetLoc;

    private boolean usingAStar = false;

    public void moveRandom(){
        Direction direction = Util.getRandomDirection();
        if (gc.isMoveReady(thisUnit.id()) && gc.canMove(thisUnit.id(), direction)) {
            gc.moveRobot(thisUnit.id(), direction);
        }
    }


    //highAccuracy bool will only set a new target loc if its farther than 4 squares of the old
    //or if its null anyway lul
    public void setTargetLoc(MapLocation mapLocation,boolean highAccuracy){
        if (targetLoc == null ||
                ((highAccuracy && !targetLoc.equals(mapLocation)) || (!highAccuracy && mapLocation.distanceSquaredTo(targetLoc) > 16))) {
            targetLoc = mapLocation;
            usingAStar = false;
            if (thisUnit.location().mapLocation().distanceSquaredTo(targetLoc) > 100){
                generatePath();
            }
        }
    }


    public MapLocation getTargetLoc(){
        return targetLoc;
    }

    public void takeStepToTarget(){
        if (usingAStar){
            takeAStarStep();
        }else{
            fuzzyStep();
        }
    }

    public boolean completelyTrapped(){
        for (int i=1;i<=8;i++){
            if (gc.canMove(thisUnit.id(),Direction.values()[i])){
                return false;
            }
        }
        return true;
    }

    public void boardNearbyRockets(){
        VecUnit vecRockets = gc.senseNearbyUnitsByType(thisUnit.location().mapLocation(), thisUnit.visionRange(), UnitType.Rocket);

        List<Unit> rockets = new ArrayList<Unit>();
        //Don't know if this is actually required
        for (int i=0;i<vecRockets.size();i++){
            if (vecRockets.get(i).team().equals(GameConstants.myTeam)) rockets.add(vecRockets.get(i));
        }

        if(rockets.size()>0){
            Unit closestRocket = null;
            long currentClosestDist = Long.MAX_VALUE;
            for (int i = 0; i < rockets.size(); i++){
                long distToThisRocket = thisUnit.location().mapLocation().distanceSquaredTo(rockets.get(i).location().mapLocation());
                if (distToThisRocket < currentClosestDist){
                    currentClosestDist = distToThisRocket;
                    closestRocket = rockets.get(i);

                    //System.out.println(closestEnemy);
                }
            }
            if(gc.canLoad(closestRocket.id(), thisUnit.id())){
                gc.load(closestRocket.id(), thisUnit.id());
            }else{
                setTargetLoc(closestRocket.location().mapLocation(),true);
                takeStepToTarget();
            }
        }
    }



    private void generatePath(){
        targetPath = getPathTo(thisUnit,targetLoc);
        usingAStar = true;
        //System.out.println("A STAR PERFORMED");
    }

    private void fuzzyStep(){

        int[] tryRotate = {0,-1,1,-2,2};
        Direction towards = thisUnit.location().mapLocation().directionTo(targetLoc);
        if (gc.isMoveReady(thisUnit.id())){
            for (int tilt : tryRotate){
                //System.out.println("ORIGINAL INDEX" + towards.ordinal());

                int newIndex = (towards.ordinal() + tilt) < 0 ? (towards.ordinal() + tilt) + 8 : (towards.ordinal() + tilt)%8;
                //System.out.println("NEW INDEX" +newIndex);
                Direction targetDir = Direction.values()[newIndex];
                if (gc.canMove(thisUnit.id(),targetDir)){
                    gc.moveRobot(thisUnit.id(),targetDir);
                    return;
                }
            }
            //no move possible with fuzzyStep;;do an A*
            if (!completelyTrapped()) generatePath();
        }

    }

    private void takeAStarStep(){
        //System.out.println("Attempting to take step "+ thisUnit.id());
        if (targetPath.size()>0) {
            MapLocation targetPathLoc = targetPath.get(0);
            Direction targetDir = thisUnit.location().mapLocation().directionTo(targetPathLoc);
            if (gc.isMoveReady(thisUnit.id()) && gc.canMove(thisUnit.id(), targetDir)) {
                gc.moveRobot(thisUnit.id(), targetDir);
                targetPath.remove(0);
            }
        }
    }





    private ArrayList<MapLocation> getPathTo(Unit unit,MapLocation targetLoc){
        long startTime = System.currentTimeMillis();

        ArrayList<MapLocation> path = new ArrayList<MapLocation>();


        //Check if its a valid location first
        PlanetMap pm;
        if (targetLoc.getPlanet().equals(Planet.Earth)) pm = GameConstants.earthPM;
        else{
            pm = GameConstants.marsPM;
        }

        if (pm.isPassableTerrainAt(targetLoc) == 1){

            PriorityQueue<AStarMapLoc> openList = new PriorityQueue<>(A_STAR_MAP_LOC_COMPARATOR);
            HashSet<AStarMapLoc> closedList = new HashSet<AStarMapLoc>();

            MapLocation unitMapLoc = unit.location().mapLocation();
            AStarMapLoc unitAStarMapLoc = new AStarMapLoc(unitMapLoc.getPlanet(),unitMapLoc.getX(),unitMapLoc.getY());

            //add start node to open list
            unitAStarMapLoc.g = 0;
            unitAStarMapLoc.h = unitAStarMapLoc.distanceSquaredTo(targetLoc);
            unitAStarMapLoc.f = unitAStarMapLoc.g + unitAStarMapLoc.h;
            openList.add(unitAStarMapLoc);

            while(!openList.isEmpty()){

                //get the current node
                AStarMapLoc currentNode = openList.poll();
                //System.out.println("POLLED F VALUE" + currentNode.f);

                closedList.add(currentNode);

                //  System.out.println("Open List size:" + openList.size());
                //  System.out.println("Closed List size: "+ closedList.size());

                //Found the goal! WOOOOOOOOO LETS GOO BOYS
                if (currentNode.mapLocation().equals(targetLoc) ||
                        System.currentTimeMillis() - startTime > 100){
                    if ( System.currentTimeMillis() - startTime > 100) System.out.println("A* timeout");
                    AStarMapLoc current = currentNode;
                    while (current != null){
                        path.add(current);
                        current = current.parent;
                    }
                    Collections.reverse(path);
                    path.remove(0); //remove the unit's starting square
                    break;
                }


                //get children of node
                HashSet<AStarMapLoc> children = currentNode.getNeighbors();

                for (AStarMapLoc child : children){

                    boolean doContinue = false;
                    for (AStarMapLoc closedNode : closedList){
                        if (closedNode.mapLocation().equals(child.mapLocation())){
                            doContinue = true;
                        }
                    }
                    if (doContinue) continue;
                    if (doContinue) System.out.println("THIS SHOULD NEVEr ruN");
                    //Create f g h value
                    child.g = currentNode.g + currentNode.distanceSquaredTo(child);
                    child.h = child.distanceSquaredTo(targetLoc);
                    child.f = child.g + child.h;

                    doContinue = false;
                    for (AStarMapLoc openNode : openList){
                        if (openNode.mapLocation().equals(child.mapLocation())){
                            if (child.g > openNode.g){
                                doContinue = true;
                            }
                        }
                    }
                    if (doContinue) continue;

                    openList.add(child);
                }



            }


        }else{
            //not a valid target
            throw new IllegalArgumentException("Unit "+unit.id()+" attempted to path to an invalid loc. Round:"+gc.round());

        }
        return path;
    }



}
