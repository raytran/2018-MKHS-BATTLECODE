import bc.*;

class BotKnight extends BotBase{
    BotKnight(GameController theGC){
        super(theGC);
    }
    void loop(){

        if(thisUnit.location().isOnMap()){
            /** ATTACKING **/
            VecUnit enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.attackRange(), GameConstants.enemyTeam);
            if(enemies.size()>0){
                //for some reason they stopped shooting suddenly on mars
                for(int i = 0; i < enemies.size(); i++){
                    if(gc.isAttackReady(thisUnit.id()) && gc.canAttack(thisUnit.id(), enemies.get(i).id())){
                        gc.attack(thisUnit.id(), enemies.get(i).id());
                        break;
                    }
                }

            }
            /** MOVING **/
            if (!completelyTrapped()) {
                if (thisUnit.location().isOnPlanet(Planet.Earth)) {
                    if (!boardNearbyRockets()) {

                        //enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.visionRange(), Player.enemyTeam);
                        //go to closest enemy
                        Unit closestEnemy = null;
                        long currentClosestDist = Long.MAX_VALUE;
                        for (Unit enemyUnit : Player.enemiesOnMapInVisionRange) {
                            long distToThisEnemy = thisUnit.location().mapLocation().distanceSquaredTo(enemyUnit.location().mapLocation());
                            if (distToThisEnemy < currentClosestDist) {
                                currentClosestDist = distToThisEnemy;
                                closestEnemy = enemyUnit;

                                //System.out.println(closestEnemy);
                            }
                        }


                        if (closestEnemy != null && gc.getTimeLeftMs() > 5000) {
                            setTargetLoc(closestEnemy.location().mapLocation());
                            takeStepToTarget();
                        } else {
                            //System.out.println("USING CACHE");
                            goToEnemyStartUsingBFSCache();
                        }
                    }
                } else {

                    //enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.visionRange(), Player.enemyTeam);
                    //go to closest enemy
                    Unit closestEnemy = null;
                    long currentClosestDist = Long.MAX_VALUE;
                    for (Unit enemyUnit : Player.enemiesOnMapMarsInVisionRange) {
                        long distToThisEnemy = thisUnit.location().mapLocation().distanceSquaredTo(enemyUnit.location().mapLocation());
                        if (distToThisEnemy < currentClosestDist) {
                            currentClosestDist = distToThisEnemy;
                            closestEnemy = enemyUnit;

                            //System.out.println(closestEnemy);
                        }
                    }

                    if (closestEnemy != null) {
                        //System.out.println("EY KILLEM");
                        setTargetLoc(closestEnemy.location().mapLocation());
                        takeStepToTarget();
                    } else {
                        moveRandom();
                    }
                }
            }
        }

    }
}
