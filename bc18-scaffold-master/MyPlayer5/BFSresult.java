import bc.*;

import java.util.*;

class BFSresult {
    private PlanetPathLocMap planetPathLocMap;
    private HashMap<PathingMapLoc,PathingMapLoc> finalPathMap = new HashMap<>();

    BFSresult(PlanetPathLocMap planetPathLocMap1, MapLocation targetLoc){
        if (!targetLoc.getPlanet().equals(planetPathLocMap1.getPlanet()))  throw new IllegalArgumentException("BFSmap instantiated with mis-matched planets.");
        planetPathLocMap = planetPathLocMap1;
        runBFS(targetLoc);
    }

    public PathingMapLoc getPML(PathingMapLoc pathingMapLoc){
        return finalPathMap.get(pathingMapLoc);
    }



    private void runBFS(MapLocation startingLoc) {

        PathingMapLoc startingPML = new PathingMapLoc(startingLoc.getPlanet(), startingLoc.getX(), startingLoc.getY());

        if (planetPathLocMap.isLocPassable(startingPML)) {

            Queue<PathingMapLoc> frontierQueue = new ArrayDeque<>();

            //add the first element (unit pos from planetPathLocMap)
            frontierQueue.add(planetPathLocMap.getPML(startingPML));

            finalPathMap.put(startingPML, startingPML);

            while (frontierQueue.size() != 0) {
                PathingMapLoc current = frontierQueue.poll();

                PathingMapLoc currentCopy = new PathingMapLoc(current.getPlanet(), current.getX(), current.getY());

                for (PathingMapLoc next : current.getNeighbors()) {

                    if (!next.onClosedList) {
                        frontierQueue.add(next);
                        next.onClosedList = true;
                        next.parent = current;

                        PathingMapLoc newPathMapLoc = new PathingMapLoc(next.getPlanet(), next.getX(), next.getY());
                        newPathMapLoc.parent = currentCopy;
                        finalPathMap.put(newPathMapLoc, newPathMapLoc);
                    }
                }
            }


        } else {
            //not a valid target
            throw new IllegalArgumentException("BFS attempted to path to an invalid loc.");
        }
    }
}
