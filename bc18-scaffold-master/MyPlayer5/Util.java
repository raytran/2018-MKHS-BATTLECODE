/**
 * Created by raytr on 1/12/2018.
 */
import bc.*;

import java.util.Random;
import java.util.Arrays;

class Util {
    public static Direction[] DIRECTIONS = {Direction.East, Direction.North, Direction.Northeast, Direction.Northwest, Direction.South, Direction.Southeast, Direction.Southwest, Direction.West};

    static Direction getRandomDirection(){
        //do not include index0; it is center
        return Direction.values()[randIntInclusive(1,8)];
    }
    public static int randIntInclusive(int min, int max) {
        return min + (int)(Math.random() * ((max - min) + 1));
    }
    public static long randLongInclusive(long min,long max) {
        return min + (long)(Math.random() * ((max - min) + 1));
    }

    static MapLocation invertLoc(MapLocation mapLocation){
        PlanetMap pm;
        if (mapLocation.getPlanet().equals(Planet.Earth)){
            pm = GameConstants.earthPM;
        }else pm=GameConstants.marsPM;
        int newX = (int)pm.getWidth() - mapLocation.getX();
        int newY = (int)pm.getHeight() - mapLocation.getY();
        return new MapLocation(mapLocation.getPlanet(),newX,newY);
    }
}
