import bc.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * checks every 3
 * Created by raytr on 1/24/2018.
 */
class BFSmap {
    //cannot use MapLocation to index a hashMap since they are mutable
    Map<PathingMapLoc,BFSresult> bfsCalcs = new HashMap<>();
    Planet myPlanet;
    BFSmap(Planet planet) {
        myPlanet = planet;
        PlanetMap pm;
        PlanetPathLocMap pplm;
        if (planet.equals(Planet.Earth)) {
            pm = GameConstants.earthPM;
        } else {
            pm = GameConstants.marsPM;
        }

        for (int x = 0; x < pm.getWidth(); x++) {
            for (int y = 0; y < pm.getHeight(); y++) {
                if ((x-1) % 3 == 0 && (y-1) % 3 == 0) {

                    //Looks bad, but needs to be done to call the resetPathingInfo()
                    if (planet.equals(Planet.Earth)) {
                        pplm = GameConstants.getEarthPlanetPathLocMap();

                    } else {
                        pplm = GameConstants.getMarsPlanetPathLocMap();
                    }

                    //only if that square also contains neighbors that are all passable (just in case)
                    if (pplm.isLocPassable(x,y)) {

                        PathingMapLoc pathingMapLoc = pplm.getPML(x,y);
                        boolean allNeighborsPassable = true;
                        for (PathingMapLoc neighbor:pathingMapLoc.getNeighbors()){
                            if (!pplm.isLocPassable(neighbor)){
                                allNeighborsPassable = false;
                            }
                        }


                        //System.out.println("wtf");
                        if (allNeighborsPassable) {
                            MapLocation mapLocation = new MapLocation(pm.getPlanet(), x, y);
                            bfsCalcs.put(pathingMapLoc, new BFSresult(pplm, mapLocation));
                        }
                    }

                }
            }
        }
    }

    BFSresult getBFSresult(MapLocation targetLoc){
        int x = targetLoc.getX();
        int y=  targetLoc.getY();
        int newX = 3 *(x/3) +1;
        int newY = 3 * (y/3) +1;
        return bfsCalcs.get(new PathingMapLoc(myPlanet,newX,newY));
    }
    //returns true if the loc is part of a square whose center has a bfs calc
    boolean isLocInBFScache(int x,int y){
        //convert to the nearest multiple of 3 + 1(undoing the add check operation)
        int newX = 3 *(x/3) +1;
        int newY = 3 * (y/3) +1;
        return bfsCalcs.containsKey(new PathingMapLoc(myPlanet,newX,newY));
    }

    MapLocation getCenterOfBFSsquare(MapLocation mapLocation){
        int x = mapLocation.getX();
        int y=  mapLocation.getY();
        int newX = 3 *(x/3) +1;
        int newY = 3 * (y/3) +1;
        return new MapLocation(myPlanet,x,y);
    }
}
