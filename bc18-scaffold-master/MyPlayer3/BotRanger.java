/**
 * Created by raytr on 1/9/2018.
 */
import bc.*;

class BotRanger extends BotBase{
    BotRanger(GameController gc){
        super(gc);
    }
    void loop(){
        //go towards bad guys
        //make sure we're not in the factory garrison

        if(thisUnit.location().isOnMap()){
            /** ATTACKING **/
            VecUnit enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.attackRange(), GameConstants.enemyTeam);
            if(enemies.size()>0){
                //for some reason they stopped shooting suddenly on mars
                for(int i = 0; i < enemies.size(); i++){
                    if(gc.isAttackReady(thisUnit.id()) && gc.canAttack(thisUnit.id(), enemies.get(i).id())){
                        gc.attack(thisUnit.id(), enemies.get(i).id());
                        break;
                    }
                }

            }
            /** MOVING **/
            if (!completelyTrapped()) {
                if (thisUnit.location().isOnPlanet(Planet.Earth)) {
                    if (!boardNearbyRockets()) {

                        //enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.visionRange(), Player.enemyTeam);
                        //go to closest enemy
                        Unit closestEnemy = null;
                        long currentClosestDist = Long.MAX_VALUE;
                        for (Unit enemyUnit : Player.enemiesOnMapInVisionRange) {
                            long distToThisEnemy = thisUnit.location().mapLocation().distanceSquaredTo(enemyUnit.location().mapLocation());
                            if (distToThisEnemy < currentClosestDist) {
                                currentClosestDist = distToThisEnemy;
                                closestEnemy = enemyUnit;

                                //System.out.println(closestEnemy);
                            }
                        }


                        if (closestEnemy != null && gc.getTimeLeftMs() > 5000) {
                            if (closestEnemy.location().mapLocation().distanceSquaredTo(thisUnit.location().mapLocation()) <= 10)
                                return;
                            //System.out.println("EY KILLEM");
                            setTargetLoc(closestEnemy.location().mapLocation());
                            takeStepToTarget();
                            //}else if(thisUnit.location().mapLocation().distanceSquaredTo(GameConstants.startingEnemyLocation)<=thisUnit.visionRange()){
                            //    moveRandom();
                        } else {
                            //System.out.println("USING CACHE");
                            goToEnemyStartUsingBFSCache();
                        }
                    }
                } else {

                    //enemies = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.visionRange(), Player.enemyTeam);
                    //go to closest enemy
                    Unit closestEnemy = null;
                    long currentClosestDist = Long.MAX_VALUE;
                    for (Unit enemyUnit : Player.enemiesOnMapMarsInVisionRange) {
                        long distToThisEnemy = thisUnit.location().mapLocation().distanceSquaredTo(enemyUnit.location().mapLocation());
                        if (distToThisEnemy < currentClosestDist) {
                            currentClosestDist = distToThisEnemy;
                            closestEnemy = enemyUnit;

                            //System.out.println(closestEnemy);
                        }
                    }

                    if (closestEnemy != null) {
                        if (closestEnemy.location().mapLocation().distanceSquaredTo(thisUnit.location().mapLocation()) <= 10)
                            return;
                        //System.out.println("EY KILLEM");
                        setTargetLoc(closestEnemy.location().mapLocation());
                        takeStepToTarget();
                    } else {
                        moveRandom();
                    }
                }
            }
        }

    }
}
