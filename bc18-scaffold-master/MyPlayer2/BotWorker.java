import bc.*;

import java.util.HashSet;
import java.util.Set;

class BotWorker extends BotBase{
    BotWorker(GameController theGC){
        super(theGC);
    }

    private MapLocation bestKarboniteTarget = null;

    void loop(){
        if (thisUnit.location().isOnMap()) {

            VecUnit nearbyFriends = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(),thisUnit.visionRange(),GameConstants.myTeam);
            Set<Unit> offensiveFriends = new HashSet<>();
            for (int i = 0; i < nearbyFriends.size(); i++) {
                Unit unit = nearbyFriends.get(i);
                UnitType thisType = unit.unitType();
                if (thisType != UnitType.Worker) {
                    if (thisType != UnitType.Factory
                            && thisType != UnitType.Rocket) {
                        offensiveFriends.add(unit);
                    }
                }
            }

            VecUnit nearbyFriendsCloser = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(),9,GameConstants.myTeam);
            Set<Unit> nearbyStructures = new HashSet<>();
            for (int i=0;i<nearbyFriendsCloser.size();i++){
                Unit unit = nearbyFriendsCloser.get(i);
                UnitType thisType = unit.unitType();
                if (thisType == UnitType.Factory || thisType == UnitType.Rocket){
                    nearbyStructures.add(unit);
                }
            }





            VecUnit adjacentUnits = gc.senseNearbyUnits(thisUnit.location().mapLocation(), 2);
            //prioritize getting factories finished
            /** BLUEPRINT BUILDING **/
            for (int i = 0; i < adjacentUnits.size(); i++) {
                Unit unit = adjacentUnits.get(i);
                if (unit.unitType().equals(UnitType.Rocket) || unit.unitType().equals(UnitType.Factory)) {
                    //if its not finished building, then help build
                    //System.out.println("Unit Type:" + unit.unitType());
                    if (unit.structureIsBuilt() == 0) {
                        if (gc.canBuild(thisUnit.id(), unit.id())) {
                            gc.build(thisUnit.id(), unit.id());
                            return; //exit the loop early
                        }
                    }
                }
            }

            //go to closest unbuilt structure
            Unit closestUnbuiltStructure = null;
            long currentClosestDist = Long.MAX_VALUE;

            for (Unit structure : nearbyStructures) {
                long distToThisStructure = thisUnit.location().mapLocation().distanceSquaredTo(structure.location().mapLocation());
                if (distToThisStructure < currentClosestDist && structure.structureIsBuilt() == 0) {
                    currentClosestDist = distToThisStructure;
                    closestUnbuiltStructure = structure;
                    //System.out.println(closestEnemy);
                }
            }
            if (closestUnbuiltStructure != null){
                setTargetLoc(closestUnbuiltStructure.location().mapLocation());
                takeStepToTarget();
                return; // exit the loop early
            }



            /** KARBONITE **/
            if (thisUnit.location().isOnPlanet(Planet.Earth)) {
                try {
                    //justify generating a new target
                    if (
                        //when we don't have an initial one
                            bestKarboniteTarget == null ||
                                    // when another unit is at the loc
                                    (!thisUnit.location().mapLocation().equals(bestKarboniteTarget)
                                            && gc.hasUnitAtLocation(bestKarboniteTarget))
                                    //when we expended the karbonite at the current one
                                    || (gc.karboniteAt(bestKarboniteTarget) == 0)) {
                        //just check for closest; gc functions do not work outside of vision range
                        bestKarboniteTarget = getNewKarboniteTarget();
                    }
                } catch (Exception e) {
                    //Location not visible when calling gc.hasUnitAt()
                    //this just means our current one is too far to call the gc functions
                    //keep going
                }
            }

            //just move around away from nearby units and mine
            if (gc.karboniteAt(thisUnit.location().mapLocation()) == 0) {
                if (!completelyTrapped()) {
                    if (bestKarboniteTarget != null) {
                        setTargetLoc(bestKarboniteTarget);
                        takeStepToTarget();
                    } else {
                        moveRandom();
                    }
                }
            } else {
                //System.out.println("AT KARBONITE TARGET");
                if (gc.canHarvest(thisUnit.id(), Direction.Center)) {
                    //System.out.println("HARVESTING");
                    gc.harvest(thisUnit.id(), Direction.Center);
                }
            }








            /** WORKER DUPLICATION **/
            if (Player.getNumWorkers() < GameConstants.EARTH_MAX_WORKERS) {
                for (Direction direction : Util.DIRECTIONS) {
                    if (gc.canReplicate(thisUnit.id(), direction)) {
                        gc.replicate(thisUnit.id(), direction);
                        return;
                    }
                }
            }

            /** BLUEPRINTING **/
            if (Player.getNumWorkers() > 0) {
                for (Direction direction : Util.DIRECTIONS) {
                    if (offensiveFriends.size() > 2 && Player.getNumFactories() >= 3) {
                        if (gc.canBlueprint(thisUnit.id(), UnitType.Rocket, direction)) {
                            gc.blueprint(thisUnit.id(), UnitType.Rocket, direction);
                            return;
                        }
                    } else if (gc.canBlueprint(thisUnit.id(), UnitType.Factory, direction)) {
                        gc.blueprint(thisUnit.id(), UnitType.Factory, direction);
                        return;
                    }
                }
            }

        }

    }




    private MapLocation getNewKarboniteTarget(){
        MapLocation kbTarget = null;
        long currentClosestDist = Long.MAX_VALUE;

        //get closest in nearbySquares
        VecMapLocation nearbySquares = gc.allLocationsWithin(thisUnit.location().mapLocation(),thisUnit.visionRange());
        for (int i=0;i<nearbySquares.size();i++){
            MapLocation thisSquare = nearbySquares.get(0);
            long distToSquare = thisSquare.distanceSquaredTo(thisUnit.location().mapLocation());

            //handle ties by looking at greater karbonite
            if (distToSquare == currentClosestDist){
                if (!gc.hasUnitAtLocation(thisSquare)){
                    if (gc.karboniteAt(thisSquare) > gc.karboniteAt(kbTarget)) kbTarget = thisSquare;
                }
            } else if ((distToSquare < currentClosestDist)
                    && !gc.hasUnitAtLocation(thisSquare)
                    && gc.karboniteAt(thisSquare) > 0) {
                kbTarget = thisSquare;
                currentClosestDist = distToSquare;
            }
        }

        //if its still null, there are no karbonite targets in our unit's vision
        //get the target from the mapwide vision
        if (kbTarget == null) {
            //System.out.println("Running expensive karonite search");
            for (MapLocation thisSquare : Player.earthKarboniteLocs) {
                long distToSquare = thisSquare.distanceSquaredTo(thisUnit.location().mapLocation());
                try {
                    //handle ties by looking at greater karbonite
                    if (distToSquare == currentClosestDist){
                        if (!gc.hasUnitAtLocation(thisSquare)){
                            if (gc.karboniteAt(thisSquare) > gc.karboniteAt(kbTarget)) kbTarget = thisSquare;
                        }
                    } else if ((distToSquare < currentClosestDist)
                            && !gc.hasUnitAtLocation(thisSquare)
                            && gc.karboniteAt(thisSquare) > 0) {
                        kbTarget = thisSquare;
                        currentClosestDist = distToSquare;
                    }
                } catch (Exception e) {
                    //Location not visible; check distance only
                    if ((distToSquare < currentClosestDist)) {
                        kbTarget = thisSquare;
                        currentClosestDist = distToSquare;
                    }
                }
            }
        }else{
            //System.out.println("Normal karbonite search OK");
        }
        return kbTarget;
    }

}
