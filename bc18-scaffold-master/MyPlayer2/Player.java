import bc.*;
import java.util.*;

public class Player {
    public static HashSet<MapLocation> earthKarboniteLocs = new HashSet<MapLocation>();
    public static HashSet<Unit> enemiesOnMapInVisionRange = new HashSet<Unit>();
    public static HashSet<Unit> enemiesOnMapMarsInVisionRange = new HashSet<Unit>();
    public static Map<Integer,UnitBase> myUnitsHash = new HashMap<Integer,UnitBase>();

    private static int numFactories = 0;
    private static int numRockets = 0;
    private static int numWorkers = 0;
    private static int numKnights = 0;
    private static int numRangers = 0;
    private static int numMages = 0;
    private static int numHealers = 0;
    private static double workerToFactoryRatio = 0.0;
    public static GameController gc;

    public static void main(String[] args) {
        gc = new GameController();

        //initialize variables that that won't ever change (GameConstants)
        GameConstants.init(gc);

        //initialize variables that WILL change (Player variables)
        initVars();

        //Start Research
        queuePlayerResearch();


        //Start the rounds!
        while (true) {
            System.out.println("ROUND: "+gc.round()+" TIME LEFT: " + gc.getTimeLeftMs());
            //Update karbonite info
            updateEarthKarboniteLocs();

            //Update our enemy vision
            VecUnit defaultAllVecUnit = gc.units();
            updateEnemyVision(defaultAllVecUnit);

            //update our unit data
            VecUnit defaultMyVecUnit = gc.myUnits();
            updateMyUnitHashAndCount(defaultMyVecUnit);

            //Run unit code
            for (UnitBase unit:myUnitsHash.values()){
                unit.loop();
                unit.numTurnsAlive++;


            }



            if (gc.round() % 10 == 0){
                System.runFinalization();
                System.gc();
            }




            // Submit the actions we've done, and wait for our next turn.
            gc.nextTurn();
        }
    }

    private static void initVars(){
        //set init earth karbonite targets
        for (int x=0;x<=GameConstants.earthPM.getWidth();x++){
            for (int y=0;y<=GameConstants.earthPM.getHeight();y++){
                MapLocation thisLoc = new MapLocation(Planet.Earth,x,y);
                if (GameConstants.earthPM.onMap(thisLoc) && GameConstants.earthPM.initialKarboniteAt(thisLoc) > 0){
                    earthKarboniteLocs.add(thisLoc);
                }
            }
        }
        System.out.println("INIT EARTH KARBONITE TARGET SIZE"+ earthKarboniteLocs.size());
    }

    private static void queuePlayerResearch(){
        gc.queueResearch(UnitType.Rocket);
        gc.queueResearch(UnitType.Worker);
        gc.queueResearch(UnitType.Ranger);
        gc.queueResearch(UnitType.Healer);
        gc.queueResearch(UnitType.Worker);
        gc.queueResearch(UnitType.Ranger);
        gc.queueResearch(UnitType.Healer);
        gc.queueResearch(UnitType.Rocket);
        gc.queueResearch(UnitType.Rocket);


    }

    private static void updateMyUnitHashAndCount(VecUnit defaultMyVecUnit){
        //if this is a new unit, add the id to the hash & generate a new bot controller class
        for (int i =0;i<defaultMyVecUnit.size();i++){
            Unit unit = defaultMyVecUnit.get(i);
            if (!myUnitsHash.containsKey(unit.id())){
                UnitBase newUnit;
                switch (unit.unitType()){
                    case Factory:
                        newUnit = new StructureFactory(gc);
                        numFactories++;
                        break;
                    case Rocket:
                        newUnit = new StructureRocket(gc);
                        numRockets++;
                        break;
                    case Worker:
                        newUnit = new BotWorker(gc);
                        numWorkers++;
                        break;
                    case Knight:
                        newUnit = new BotKnight(gc);
                        numKnights++;
                        break;
                    case Ranger:
                        newUnit = new BotRanger(gc);
                        numRangers++;
                        break;
                    case Mage:
                        newUnit = new BotMage(gc);
                        numMages++;
                        break;
                    case Healer:
                        newUnit = new BotHealer(gc);
                        numHealers++;
                        break;
                    default:
                        throw new IllegalArgumentException("some sort of alien robot type");
                }
                myUnitsHash.put(unit.id(),newUnit);
            }

            //update each robot's /unit/ object
            myUnitsHash.get(unit.id()).setUnit(unit);

        }
        //if the hashmap contains keys that the VecUnit mydefaultVecUnit does not have
        //then these bots are dead; remove them from the hashmap to save space
        Set<Integer> keysToRemove = new HashSet<Integer>();
        for (Integer key : myUnitsHash.keySet()){
            try{
                gc.unit(key);
            }catch (RuntimeException ex){
                //unit dosen't exist; delete from hash
                keysToRemove.add(key);
                UnitBase thisWrapper = myUnitsHash.get(key);
                if (thisWrapper instanceof StructureFactory) numFactories--;
                else if (thisWrapper instanceof StructureRocket) numRockets--;
                else if (thisWrapper instanceof BotWorker) numWorkers--;
                else if (thisWrapper instanceof BotKnight) numKnights--;
                else if (thisWrapper instanceof BotRanger) numRangers--;
                else if (thisWrapper instanceof BotMage) numMages--;
                else if (thisWrapper instanceof BotHealer) numHealers--;
            }
        }
        myUnitsHash.keySet().removeAll(keysToRemove);

        if (numFactories > 0){
            workerToFactoryRatio = (double) numWorkers/ numFactories;
        }else{
            workerToFactoryRatio = Double.MAX_VALUE;
        }
    }

    private static void updateEnemyVision(VecUnit defaultAllVecUnit){
        //reset it from last round
        enemiesOnMapInVisionRange.clear();
        enemiesOnMapMarsInVisionRange.clear();
        //repopulate
        for (int i=0;i<defaultAllVecUnit.size();i++){
            Unit unit = defaultAllVecUnit.get(i);
            if (unit.team().equals(GameConstants.enemyTeam) && unit.location().isOnMap() && unit.location().isOnPlanet(Planet.Earth)) enemiesOnMapInVisionRange.add(unit);
            if (unit.team().equals(GameConstants.enemyTeam) && unit.location().isOnMap() && unit.location().isOnPlanet(Planet.Mars)) enemiesOnMapMarsInVisionRange.add(unit);
        }
        //System.out.println(enemiesOnMapInVisionRange.size());
    }

    private static void updateEarthKarboniteLocs(){
        //remove stuff as karbonite is depleted
        HashSet<MapLocation> locsToRemove = new HashSet<MapLocation>();
        for (MapLocation thisSquare : Player.earthKarboniteLocs){
            try{
                if (gc.karboniteAt(thisSquare) == 0) locsToRemove.add(thisSquare);
            }catch (Exception e){
                //outside of vision range
            }
        }
        Player.earthKarboniteLocs.removeAll(locsToRemove);
    }

    public static int getNumFactories() {
        return numFactories;
    }

    public static int getNumRockets() {
        return numRockets;
    }

    public static int getNumHealers() {
        return numHealers;
    }

    public static int getNumMages() {
        return numMages;
    }

    public static int getNumRangers() {
        return numRangers;
    }

    public static int getNumKnights() {
        return numKnights;
    }

    public static int getNumWorkers() {
        return numWorkers;
    }

    public static double getWorkerToFactoryRatio(){
        return workerToFactoryRatio;
    }
}