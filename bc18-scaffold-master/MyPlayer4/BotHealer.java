import bc.*;

class BotHealer extends BotBase{
    BotHealer(GameController theGC){
        super(theGC);
    }

    void loop(){
        //go to nearby friends
        if (thisUnit.location().isOnMap()) {
            /** HEALING **/
            VecUnit nearbyTeammates = gc.senseNearbyUnitsByTeam(thisUnit.location().mapLocation(), thisUnit.attackRange(), GameConstants.myTeam);
            healNearbyUnits(nearbyTeammates);


            /** MOVING **/
            if (!completelyTrapped()) {
                if (thisUnit.location().isOnPlanet(Planet.Earth)) {
                    boardNearbyRockets();
                }


                Unit closestFriend = null;
                long currentClosestDist = Long.MAX_VALUE;
                for (UnitBase friend : Player.myUnitsHash.values()) {
                    //for now only heal rangers
                    Unit friendlyUnit = friend.thisUnit;
                    if (friendlyUnit.location().isOnMap() && (friendlyUnit.unitType().equals(UnitType.Knight) || friendlyUnit.unitType().equals(UnitType.Ranger))) {
                        long distToThisFriend = thisUnit.location().mapLocation().distanceSquaredTo(friend.thisUnit.location().mapLocation());
                        if (distToThisFriend < currentClosestDist) {
                            currentClosestDist = distToThisFriend;
                            closestFriend = friend.thisUnit;
                        }
                    }
                }
                if (closestFriend != null) {
                    BotBase closestFriendWrap = (BotBase) Player.myUnitsHash.get(closestFriend.id());
                    if (closestFriendWrap.getTargetLoc() != null){
                        if (thisUnit.location().mapLocation().distanceSquaredTo(closestFriendWrap.getTargetLoc()) > 9) {
                            setTargetLoc(closestFriendWrap.getTargetLoc());
                            takeStepToTarget();
                        }else{
                            moveRandom();
                        }
                    } else {
                        if (currentClosestDist > 9) {
                            setTargetLoc(closestFriend.location().mapLocation());
                            takeStepToTarget();
                        }else{
                            moveRandom();
                        }
                    }
                }else{
                    if (thisUnit.location().isOnPlanet(Planet.Earth)) {
                        goToEnemyStartUsingBFSCache();
                    }else{
                        moveRandom();
                    }
                }
            }
        }


    }

    private void healNearbyUnits(VecUnit nearbyTeammates){
        long currentLowestTeammateHealth = Long.MAX_VALUE;
        Unit healTarget = null;
        if (nearbyTeammates.size() > 0) {
            for (int i = 0; i < nearbyTeammates.size(); i++) {
                Unit thisUnit = nearbyTeammates.get(i);
                if (thisUnit.unitType() != UnitType.Factory && thisUnit.unitType() != UnitType.Rocket && thisUnit.health() < currentLowestTeammateHealth){
                    currentLowestTeammateHealth = thisUnit.health();
                    healTarget = thisUnit;
                }
            }
        }
        if (healTarget != null && gc.canHeal(thisUnit.id(),healTarget.id()) && gc.isHealReady(thisUnit.id())) {
            gc.heal(thisUnit.id(),healTarget.id());
            //System.out.println("HEALED");
        }
    }


}
