import bc.*;

import java.util.*;

class BotBase extends UnitBase{
    BotBase(GameController theGC){
        super(theGC);
    }

    private ArrayList<MapLocation> targetPath;
    private MapLocation targetLoc;


    public void moveRandom(){
        Direction direction = Util.getRandomDirection();
        if (gc.isMoveReady(thisUnit.id()) && gc.canMove(thisUnit.id(), direction)) {
            gc.moveRobot(thisUnit.id(), direction);
        }
    }


    //highAccuracy bool will only set a new target loc if its farther than 4 squares of the old
    //or if its null anyway lul
    public void setTargetLoc(MapLocation mapLocation){


        if (targetLoc == null || (gc.round() % 4 ==0 && !targetLoc.equals(mapLocation))){
            targetLoc = mapLocation;

            generatePath();

        }

    }


    public MapLocation getTargetLoc(){
        return targetLoc;
    }

    public ArrayList<MapLocation> getTargetPath(){
        return targetPath;
    }
    public void takeStepToTarget(){
        takePathStep();
    }

    public boolean completelyTrapped(){
        for (int i=1;i<=8;i++){
            if (gc.canMove(thisUnit.id(),Direction.values()[i])){
                return false;
            }
        }
        return true;
    }

    //true if there are rocket targets
    public boolean boardNearbyRockets(){
        VecUnit vecRockets = gc.senseNearbyUnitsByType(thisUnit.location().mapLocation(), thisUnit.visionRange(), UnitType.Rocket);

        List<Unit> rockets = new ArrayList<Unit>();
        //Don't know if this is actually required
        for (int i=0;i<vecRockets.size();i++){
            Unit thisRocket = vecRockets.get(i);
            if (thisRocket.structureIsBuilt() == 1 && thisRocket.team().equals(GameConstants.myTeam)) rockets.add(vecRockets.get(i));
        }

        if(rockets.size()>0){
            Unit closestRocket = null;
            long currentClosestDist = Long.MAX_VALUE;
            for (int i = 0; i < rockets.size(); i++){
                long distToThisRocket = thisUnit.location().mapLocation().distanceSquaredTo(rockets.get(i).location().mapLocation());
                if (distToThisRocket < currentClosestDist){
                    currentClosestDist = distToThisRocket;
                    closestRocket = rockets.get(i);

                    //System.out.println(closestEnemy);
                }
            }
            if(gc.canLoad(closestRocket.id(), thisUnit.id())){
                gc.load(closestRocket.id(), thisUnit.id());
            }else{
                setTargetLoc(closestRocket.location().mapLocation());
                takeStepToTarget();
            }
            return true;
        }
        return false;
    }


    public void goToEnemyStartUsingBFSCache(){
       // System.out.println("USING BFS WALK");
        Planet planet = thisUnit.location().mapLocation().getPlanet();
        int x = thisUnit.location().mapLocation().getX();
        int y = thisUnit.location().mapLocation().getY();
        //get the current square
        PathingMapLoc currentSquare = GameConstants.BFStoEnemyStart.get(new PathingMapLoc(planet,x,y));
        //get the next target
        //we need to reverse the parent (BFS starts from start, goes to end)


        if (currentSquare!=null && currentSquare.parent!=null) fuzzyStep(currentSquare.parent.mapLocation());
        else {
            Direction targetDir = thisUnit.location().mapLocation().directionTo(GameConstants.startingEnemyLocation);
            if (gc.isMoveReady(thisUnit.id()) && gc.canMove(thisUnit.id(), targetDir)) {
                gc.moveRobot(thisUnit.id(), targetDir);
            }
        }
    }

    private void generatePath(){

        targetPath = getBFSPathTo(thisUnit.location().mapLocation(),targetLoc);

    }

    //returns true if you moved
    //false if you didn't
    private boolean fuzzyStep(MapLocation targetLoc){

        int[] tryRotate = {0,-1,1,-2,2};
        Direction towards = thisUnit.location().mapLocation().directionTo(targetLoc);
        if (gc.isMoveReady(thisUnit.id())){
            for (int tilt : tryRotate){
                //System.out.println("ORIGINAL INDEX" + towards.ordinal());
                int newIndex = (towards.ordinal() + tilt) < 0 ? (towards.ordinal() + tilt) + 8 : (towards.ordinal() + tilt)%8;
                //System.out.println("NEW INDEX" +newIndex);
                Direction targetDir = Direction.values()[newIndex];
                if (gc.canMove(thisUnit.id(),targetDir)){
                    gc.moveRobot(thisUnit.id(),targetDir);
                    return true;
                }
            }

        }
        return false;

    }


    private void takePathStep(){
        //System.out.println("Attempting to take step "+ thisUnit.id());
        if (targetPath.size()>0) {
            //path sidestep; make a new path -- don't call another generate() prob. don't need it
            if (fuzzyStep(targetPath.get(0))) {
                targetPath.remove(0);
            }

            //MapLocation targetPathLoc = targetPath.get(0);
            //Direction targetDir = thisUnit.location().mapLocation().directionTo(targetPathLoc);
            //if (gc.isMoveReady(thisUnit.id()) && gc.canMove(thisUnit.id(), targetDir)) {
            //    gc.moveRobot(thisUnit.id(), targetDir);
            //    targetPath.remove(0);
            //}
        }
    }


    private ArrayList<MapLocation> getBFSPathTo(MapLocation startingLoc,MapLocation targetLoc){
        ArrayList<MapLocation> path = new ArrayList<MapLocation>();
        //Check if its a valid location first
        Map<PathingMapLoc,PathingMapLoc> pathingMap;
        if (targetLoc.getPlanet().equals(Planet.Earth)){
            pathingMap = GameConstants.getEarthPathingMap();
        }
        else {
            pathingMap = GameConstants.getMarsPathingMap();
        }

        PathingMapLoc startingPML = new PathingMapLoc(startingLoc.getPlanet(),startingLoc.getX(),startingLoc.getY());
        PathingMapLoc targetPML = new PathingMapLoc(targetLoc.getPlanet(),targetLoc.getX(),targetLoc.getY());
        if (pathingMap.containsKey(targetPML)) {

            Queue<PathingMapLoc> frontierQueue = new ArrayDeque<>();

            //add the first element (unit pos from pathingMap)
            frontierQueue.add(pathingMap.get(startingPML));

            while (frontierQueue.size() !=0){
                //System.out.println("WE HERE");

                PathingMapLoc current = frontierQueue.poll();

                if (current.equals(targetPML)){

                    while (!current.equals(startingPML)){
                        //System.out.println(current);
                        path.add(current.mapLocation());
                        current = current.parent;
                    }
                    Collections.reverse(path);
                    //path.remove(0); //remove the unit's starting square
                    break;
                }



                for (PathingMapLoc next: current.getNeighbors()){


                    if (!next.onClosedList ){
                        frontierQueue.add(next);
                        next.onClosedList = true;
                        next.parent = current;
                    }
                }
            }



        } else {
            //not a valid target
            throw new IllegalArgumentException("BFS attempted to path to an invalid loc.");

        }
        return path;
    }


    public static HashMap<PathingMapLoc,PathingMapLoc> runFullBFS(MapLocation startingLoc) {
        HashMap<PathingMapLoc,PathingMapLoc> finalPathMap = new HashMap<>();

        //Check if its a valid location first
        Map<PathingMapLoc,PathingMapLoc> pathingMap;
        if (startingLoc.getPlanet().equals(Planet.Earth)){
            pathingMap = GameConstants.getEarthPathingMap();
        }else{
            pathingMap = GameConstants.getMarsPathingMap();
        }


        PathingMapLoc startingPML = new PathingMapLoc(startingLoc.getPlanet(),startingLoc.getX(),startingLoc.getY());

        if (pathingMap.containsKey(startingPML)) {


            Queue<PathingMapLoc> frontierQueue = new ArrayDeque<>();

            //add the first element (unit pos from pathingMap)
            frontierQueue.add(pathingMap.get(startingPML));

            finalPathMap.put(startingPML,startingPML);

            while (frontierQueue.size() !=0){
                PathingMapLoc current = frontierQueue.poll();

                PathingMapLoc currentCopy = new PathingMapLoc(current.getPlanet(),current.getX(),current.getY());

                for (PathingMapLoc next: current.getNeighbors()){

                    if (!next.onClosedList){
                        frontierQueue.add(next);
                        next.onClosedList = true;
                        next.parent = current;

                        PathingMapLoc newPathMapLoc = new PathingMapLoc(next.getPlanet(),next.getX(),next.getY());
                        newPathMapLoc.parent = currentCopy;
                        finalPathMap.put(newPathMapLoc,newPathMapLoc);
                    }
                }
            }



        } else {
            //not a valid target
            throw new IllegalArgumentException("BFS attempted to path to an invalid loc.");

        }
        return finalPathMap;
    }

    public Unit getClosestEnemyInMapVision(){
        Set<Unit> enemies;
        if (thisUnit.location().isOnPlanet(Planet.Earth)){
            enemies = Player.enemiesOnMapInVisionRange;
        }else{
            enemies = Player.enemiesOnMapMarsInVisionRange;
        }

        Unit closestEnemy = null;
        long currentClosestDist = Long.MAX_VALUE;
        for (Unit enemyUnit : enemies) {
            long distToThisEnemy = thisUnit.location().mapLocation().distanceSquaredTo(enemyUnit.location().mapLocation());
            if (distToThisEnemy < currentClosestDist) {
                currentClosestDist = distToThisEnemy;
                closestEnemy = enemyUnit;

                //System.out.println(closestEnemy);
            }
        }
        return closestEnemy;
    }

}
